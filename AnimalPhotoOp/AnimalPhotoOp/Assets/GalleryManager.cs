﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GalleryManager : MonoBehaviour
{


    [SerializeField]
    GameObject[] GalleryBlocks;


    public void CheckListUpdate()
    {
        for (int i = 0; i < GalleryBlocks.Length; i++)
        {
            if (GetComponent<PlayerVariables>().foundAnimals[i])
            {
                GalleryBlocks[i].GetComponent<Image>().color = Color.white;
            }
        }
    }

}

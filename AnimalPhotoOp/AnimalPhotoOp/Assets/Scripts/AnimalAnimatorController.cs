﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AnimalAnimatorController : MonoBehaviour
{

    NavMeshAgent m_agent;
    AnimalBehaviour m_behaviour;
    Animator m_Animator;
    AnimalBaseScriptableObject m_Variables;
    private void Awake()
    {
        m_behaviour = GetComponent<AnimalBehaviour>();
        m_Animator = GetComponent<Animator>();
        m_agent = GetComponent<NavMeshAgent>();
    }


    private void Update()
    {
        m_Animator.SetBool("beingPet", m_behaviour.beingPetted);
        m_Animator.SetBool("drinking", m_behaviour.drinking);
        m_Animator.SetBool("sleeping", m_behaviour.sleeping);
        m_Animator.SetFloat("currentSpeed", m_agent.velocity.magnitude);
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenShotScript : MonoBehaviour
{



    [SerializeField]
    GameObject[] buttons;

    [SerializeField]
    GameObject hoverOver;

    [SerializeField]
    UIAudio m_Audio;
    private void Update()
    {
        
        if (Input.GetButtonDown("Jump"))
        {
            hoverOver.SetActive(true);
        }
        if (Input.GetButtonUp("Jump"))
        {
            hoverOver.SetActive(false);
            StartCoroutine(CapturePhoto());
            m_Audio.playRandomAudio();
        }

    }



    public void TakeScreenShot()
    {
        StartCoroutine(CapturePhoto());
    }

    IEnumerator CapturePhoto()
    {
        string timeStamp = System.DateTime.Now.ToString("dd-MM-yyyy-HH-mm-ss");
        string fileName = "Screenshot" + timeStamp + ".png";
        string pathToSave = fileName;


        //deactivate the whole UI system it won't show up in the screenshot
        for (int i = 0; i < buttons.Length; i++)
        {
            buttons[i].SetActive(false);
        }
        
        // image is saves at Application.persistentDataPath
        ScreenCapture.CaptureScreenshot(Application.persistentDataPath + "/" +  pathToSave);
        
        
        // check the location of the screeshot
        Debug.Log(Application.persistentDataPath + "/" + pathToSave);

        // Waits until the end of the frame after Unity has rendererd every Camera and GUI, just before displaying the frame on screen.
        yield return new WaitForEndOfFrame();

        // reactivate the UI system after the screenshot been rendered.
        for (int i = 0; i < buttons.Length; i++)
        {
            buttons[i].SetActive(true);
        }


    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class EnlargeImage : MonoBehaviour
{
    // bool to detect is the image has been enlarged
    public bool isEnlarged = false;

    // vector for the original position
    Vector2 OriginalLoc;

    // vector for the positon of the pivot point
    Vector2 CenterLoc;

    [SerializeField]
    public GameObject tape;
    RectTransform m_RectTransform;
    private void Awake()
    {
        m_RectTransform = GetComponent<RectTransform>();
    }

    private void Start()
    {
        // record the original position of the canvas
        OriginalLoc = m_RectTransform.localPosition;
        
        // record the positon of the pivot point
        CenterLoc = Vector2.zero;


    }


    public void EnlargeTheImage()
    {


        if (isEnlarged == false)
        {
            tape.GetComponent<Image>().enabled = false;
            // birng the selected image to the last of the hierarchy, so it will be show at the front
            this.transform.SetAsLastSibling();

            // bring it to the center of the screen
            m_RectTransform.localPosition  = CenterLoc;

            // give the full resolution of the image

            this.transform.localScale = new Vector3(4, 4, 1);

            // change bool to ture
            isEnlarged =! isEnlarged;

            // kill the funtion, so it don't excute the next if sentence.
            return;

        }

        
        if (isEnlarged == true)
        {
            tape.GetComponent<Image>().enabled = true;
            ResetImage();
        
            return;

        }

    }

    public void ResetImage()
    {
        // birng the selected image to the last of the hierarchy, so it will be show at the front
        this.transform.SetSiblingIndex(1);

        // bring it to the center of the screen
        m_RectTransform.localPosition = OriginalLoc;

        // bring the image back to its original size
        this.transform.localScale = new Vector3(1, 1, 1);

        

        // change bool to false
        isEnlarged = !isEnlarged;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Gallery_LoadScene : MonoBehaviour
{
    [SerializeField]
    string gallerySceneName;

    [SerializeField]
    string ScrapBookSceneName;

    public void LoadGalleryScene()
    {
        SceneManager.LoadScene(gallerySceneName);
    }

    public void LoadScrapBook()
    {
        SceneManager.LoadScene(ScrapBookSceneName);
    }


}

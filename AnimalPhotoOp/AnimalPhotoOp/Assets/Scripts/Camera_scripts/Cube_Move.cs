﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cube_Move : MonoBehaviour
{
    
    // move the cube for testing the camera

    private void FixedUpdate()
    {
        // move the cube along the x axils
        this.transform.position = new Vector3(transform.position.x + Time.deltaTime * 3, transform.position.y, transform.position.z);

        // reverst the direction on x axils
        if (transform.position.x > 13)
            transform.position = new Vector3(0, transform.position.y, transform.position.z);
    }
}

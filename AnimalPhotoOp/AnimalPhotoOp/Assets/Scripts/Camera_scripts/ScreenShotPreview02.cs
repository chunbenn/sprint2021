﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using UnityEngine.SceneManagement;

public class ScreenShotPreview02 : MonoBehaviour
{


    // setup Canvas to hold the screenshots
    [SerializeField]
    GameObject canvas01;
    [SerializeField]
    GameObject canvas02;
    [SerializeField]
    GameObject canvas03;
    [SerializeField]
    GameObject canvas04;



    string[] files = null;
    
    int whichScreenShotIsShown = 0;

    public void Start()
    {

        // this is used for testing
        Debug.Log(whichScreenShotIsShown);

        Grabfiles();
    }

    // use for intialization
    public void Grabfiles()
    {

        // get the picture file for the Application.persistentDataPath
        files = Directory.GetFiles(Application.persistentDataPath + "/", "*.png");

        // this log is for testing
        Debug.Log(files.Length);

        // Reset the varaibale to 0
        whichScreenShotIsShown = 0;

        // this log is for testing
        Debug.Log(whichScreenShotIsShown);


        // conditons is used to prevent array going out of the border when screenshot number is less than 4
        if (files.Length == 1)
        {
            GetPictureAndShowIt(canvas01);
        }

        if (files.Length == 2)
        {
            GetPictureAndShowIt(canvas01);
            GetPictureAndShowIt(canvas02);
        }

        if (files.Length == 3)
        {
            GetPictureAndShowIt(canvas01);
            GetPictureAndShowIt(canvas02);
            GetPictureAndShowIt(canvas03);
        }

        if (files.Length >= 4)
        {
            GetPictureAndShowIt(canvas01);
            GetPictureAndShowIt(canvas02);
            GetPictureAndShowIt(canvas03);
            GetPictureAndShowIt(canvas04);
        }


    }

    void GetPictureAndShowIt(GameObject CanvasNum_)
    {
        string pathToFile = files[whichScreenShotIsShown];
        Texture2D texture = GetScreenShotImage(pathToFile);
        Sprite sp = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height),
            new Vector2(0.5f, 0.5f));
        CanvasNum_.GetComponent<Image>().sprite = sp;
        whichScreenShotIsShown += 1;
        CanvasNum_.GetComponent<EnlargeImage>().tape.GetComponent<Image>().enabled = true;
    }

    Texture2D GetScreenShotImage(string filePath_)
    {
        Texture2D texture = null;

        // hold files data
        byte[] fileBytes;
        if (File.Exists(filePath_))
        {
            fileBytes = File.ReadAllBytes(filePath_);
            texture = new Texture2D(2, 2, TextureFormat.RGB24, false);
            texture.LoadImage(fileBytes);
        }

        return texture;
    }

    public void NextPicture()
    {
        if (files.Length > 4)
        {
           
            if (whichScreenShotIsShown >= files.Length)
            {
                whichScreenShotIsShown = 0;
            }
            
            GetPictureAndShowIt(canvas01);

            
            if (whichScreenShotIsShown >= files.Length)
            {
                whichScreenShotIsShown = 0;
            }

            GetPictureAndShowIt(canvas02);

            
            if (whichScreenShotIsShown >= files.Length)
            {
                whichScreenShotIsShown = 0;
            }

            GetPictureAndShowIt(canvas03);
            
            
            if (whichScreenShotIsShown >= files.Length)
            {
                whichScreenShotIsShown = 0;
            }
            GetPictureAndShowIt(canvas04);
        }

        // this log is for testing
        Debug.Log(whichScreenShotIsShown);
        

    }


    public void PreviousPicture()
    {
        
        if (files.Length > 4)
        {
            whichScreenShotIsShown -= 8;


            if (whichScreenShotIsShown < 0)
            {
                whichScreenShotIsShown = files.Length - 4;
            }
            GetPictureAndShowIt(canvas01);

            if (whichScreenShotIsShown < 0)
            {
                whichScreenShotIsShown = files.Length - 4;
            }
            GetPictureAndShowIt(canvas02);

            if (whichScreenShotIsShown < 0)
            {
                whichScreenShotIsShown = files.Length - 4;
            }
            GetPictureAndShowIt(canvas03);

            if (whichScreenShotIsShown < 0)
            {
                whichScreenShotIsShown = files.Length - 4;
            }
            GetPictureAndShowIt(canvas04);

        }

        // this log is for testing
        Debug.Log(whichScreenShotIsShown);

        
    }


    /*
    public void BackToGameScene()
    {
        // reset the whichScreenShotIsShown
        whichScreenShotIsShown = 0;

        //reset images
        canvas01.GetComponent<EnlargeImage>().ResetImage();
        canvas02.GetComponent<EnlargeImage>().ResetImage();
        canvas03.GetComponent<EnlargeImage>().ResetImage();
        canvas04.GetComponent<EnlargeImage>().ResetImage();


    }

    */



}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasController : MonoBehaviour
{
    [SerializeField]
    GameObject[] canvasParents;


    public void turnONGameUI()
    {
        for (int i = 0; i < canvasParents.Length; i++)
        {
            canvasParents[i].SetActive(false);
        }
        canvasParents[0].SetActive(true);


    }

    public void turnOnScrapbookUI()
    {
        for (int i = 0; i < canvasParents.Length; i++)
        {
            canvasParents[i].SetActive(false);
        }
        canvasParents[1].SetActive(true);
    }

    public void turnOnGalleryUI()
    {
        for (int i = 0; i < canvasParents.Length; i++)
        {
            canvasParents[i].SetActive(false);
        }
        canvasParents[2].SetActive(true);
    }
}

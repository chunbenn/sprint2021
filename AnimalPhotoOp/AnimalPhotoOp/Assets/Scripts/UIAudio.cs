﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIAudio : MonoBehaviour
{

    [SerializeField]
    AudioClip[] m_audioClipsToPlay;
    AudioSource m_AudioSource;


    private void Awake()
    {
        m_AudioSource = GetComponentInParent<AudioSource>();
    }

    public void playAudio(int clipToPlay)
    {
        m_AudioSource.clip = m_audioClipsToPlay[clipToPlay];
        m_AudioSource.Play();
    }

    public void playRandomAudio()
    {
        m_AudioSource.clip = m_audioClipsToPlay[Random.Range(0, m_audioClipsToPlay.Length)];
        m_AudioSource.Play();
    }
}

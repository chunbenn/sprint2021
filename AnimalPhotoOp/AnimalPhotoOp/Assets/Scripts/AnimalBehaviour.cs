﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;

public class AnimalBehaviour : MonoBehaviour
{

    NavMeshAgent m_agent;
    public AnimalBaseScriptableObject AnimalValues;

    SpriteRenderer m_SpriteRenderer;

    float visitTime;
    [SerializeField]
    float currentVisitingTime;

    [SerializeField]
    float activityTime;
    float currentActivityTime;
    bool startedActivity;

    int currentWanderPoint;
    WanderPoint[] waypoints;
    Decoration[] decorations;
    Water[] WaterSources;

    public bool drinking;
    public bool sleeping;

    bool interacted = false;
    [SerializeField]
    public bool beingPetted;
    public bool wasPetted;

    LeavePoint[] LeavePoints;
    int leavePoint;

    bool intializeValues = false;

    [SerializeField]
    ParticleSystem sleepingParticles;
    [SerializeField]
    ParticleSystem happyParticles;
    [SerializeField]
    ParticleSystem musicParticles;
    Animator m_animator;


    List<GameObject> HerdMates = new List<GameObject>();
    Vector3 averageDistance;
    public bool herding;
    public bool hasHerded;
    public int animalNumber;
    public enum AnimalState
    {
        Wander, CheckOut, Drink, Idle, Sleep, Interact, Pet, GotPet, Herd, Leave
    }
    [SerializeField]
    public AnimalState currentState;

    private void Awake()
    {
        m_SpriteRenderer = GetComponent<SpriteRenderer>();
        m_agent = GetComponent<NavMeshAgent>();
        waypoints = FindObjectsOfType<WanderPoint>();
        decorations = FindObjectsOfType<Decoration>();
        WaterSources = FindObjectsOfType<Water>();
        LeavePoints = FindObjectsOfType<LeavePoint>();
        m_animator = GetComponent<Animator>();
        m_agent.SetDestination(Vector3.zero);

        leavePoint = Random.Range(0, 4);
    }
    public void SetValues()
    {
        this.gameObject.name = AnimalValues.prefabName;
        m_agent.speed = AnimalValues.moveSpeed;
        m_SpriteRenderer.sprite = AnimalValues.m_sprite;
        visitTime = AnimalValues.visitTime;
        m_animator.runtimeAnimatorController = AnimalValues.baseAnimations;
        animalNumber = AnimalValues.animalNumber;
        activityTime = AnimalValues.activityTime;
        intializeValues = true;
    }

    // Update is called once per frame
    void Update()
    {


        if (intializeValues)
        {
            if (currentVisitingTime <= visitTime)
            {
                currentVisitingTime += Time.deltaTime;
                switch (currentState)
                {
                    case AnimalState.Wander:
                        Wander();
                        break;

                    case AnimalState.CheckOut:
                        CheckOut();
                        break;

                    case AnimalState.Drink:
                        Drink();
                        break;

                    case AnimalState.Idle:
                        Idle();
                        break;

                    case AnimalState.Sleep:
                        Sleep();
                        break;

                    case AnimalState.Interact:
                        Interact();
                        break;

                    case AnimalState.Pet:
                        Pet();
                        break;

                    case AnimalState.GotPet:
                        FollowAfterPet();
                        break;

                    case AnimalState.Herd:
                        Herd();
                        break;
                    default:
                        Wander();
                        break;
                }
            }
            else
            {
                ResetStates();
                if (!herding)
                {
                    currentState = AnimalState.Leave;
                    Leave();
                }
            }
        }
    }

    void ActivityWait()
    {
        currentActivityTime += Time.deltaTime;
        if (currentActivityTime >= activityTime)
        {
            currentState = (AnimalState)Random.Range(0, 5);
            ResetStates();
        }

    }

    void ResetStates()
    {
        sleepingParticles.Stop();
        happyParticles.Stop();
        musicParticles.Stop();
        currentActivityTime = 0;
        startedActivity = false;
        sleeping = false;
        drinking = false;
        HerdMates.Clear();
        if (herding)
        {
            hasHerded = true;
            herding = false;
        }
    }
    void Wander()
    {
        ActivityWait();
        if (!startedActivity)
        {
            currentWanderPoint = Random.Range(0, waypoints.Length);
            m_agent.SetDestination(waypoints[currentWanderPoint].gameObject.transform.position);
            startedActivity = true;
        }

        if (m_agent.remainingDistance <= .5f)
        {
            float randomNumber = Random.Range(0, 2);

            if (randomNumber < 1)
            {
                currentWanderPoint = Random.Range(0, waypoints.Length);
                m_agent.SetDestination(waypoints[currentWanderPoint].gameObject.transform.position);
            }
            else
            {
                currentState = AnimalState.Idle;
            }
        }

        
    }


    void Idle()
    {
        ActivityWait();
        m_agent.SetDestination(this.transform.position);

    }

    void Sleep()
    {
        ActivityWait();
        if (!startedActivity)
        {
            sleeping = true;
            m_agent.SetDestination(this.transform.position);
            sleepingParticles.Play();
            startedActivity = true;
        }
    }


    void CheckOut()
    {
        ActivityWait();

        if (!startedActivity)
        {
            currentWanderPoint = Random.Range(0, decorations.Length);
            m_agent.SetDestination(decorations[currentWanderPoint].gameObject.transform.position);
            startedActivity = true;
        }

    }

    void Drink()
    {
        ActivityWait();

        if (!startedActivity)
        {
            drinking = true;
            currentWanderPoint = Random.Range(0, WaterSources.Length);
            m_agent.SetDestination(WaterSources[currentWanderPoint].gameObject.transform.position);
            startedActivity = true;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (sleeping)
        {
            if (other.gameObject.GetComponent<AnimalBehaviour>() != null)
            {

                if (!interacted && !other.gameObject.GetComponent<AnimalBehaviour>().interacted)
                {
                    currentState = AnimalState.Interact;
                    m_agent.SetDestination(other.transform.position);
                    interacted = true;
                }
            }
        }
    }

    void Interact()
    {
        ActivityWait();
        startedActivity = false;
        m_agent.SetDestination(this.transform.position);
    }

    private void OnMouseEnter()
    {
        ResetStates();
        currentState = AnimalState.Pet;
    }

    private void OnMouseDown()
    {
        beingPetted = true;
        wasPetted = true;
        happyParticles.Play();
        FindObjectOfType<PlayerVariables>().animalCheck(this.gameObject);
    }

    private void OnMouseUp()
    {
        beingPetted = false;
    }

    private void OnMouseExit()
    {
        if (wasPetted)
        {
            currentState = AnimalState.GotPet;
        }
        else
        {
            currentState = (AnimalState)Random.Range(0, 2);
        }
    }

    void Pet()
    {
        m_agent.SetDestination(this.transform.position);
        startedActivity = false;
        if (beingPetted)
        {
            
            //Debug.Log Do thing;
        }
        else
        {
            happyParticles.Stop();
        }
    }
    
    void FollowAfterPet()
    {
        ActivityWait();
        m_agent.SetDestination(FindObjectOfType<PlayerVariables>().worldPosition);
        wasPetted = false;
    }

    public void FoundHerdMate(GameObject foundHerdMate)
    {
        HerdMates.Add(foundHerdMate);
        currentState = AnimalState.Herd;
        currentActivityTime = 0;
        herding = true;
        musicParticles.Play();
    }

    public void Herd()
    {
        ActivityWait();

        if (HerdMates.Any())
        {
            float min_x = 0;
            float min_y = 0;
            float min_z = 0;
            float max_x = 0;
            float max_y = 0;
            float max_z = 0;

            float[] playerPositions_x = new float[HerdMates.Count];
            float[] playerPositions_y = new float[HerdMates.Count];
            float[] playerPositions_z = new float[HerdMates.Count];

            for (int i = 0; i < HerdMates.Count; i++)
            {
                playerPositions_x[i] = HerdMates[i].transform.position.x;
                playerPositions_y[i] = HerdMates[i].transform.position.x;
                playerPositions_z[i] = HerdMates[i].transform.position.x;


            }

            min_x = Mathf.Min(playerPositions_x);
            max_x = Mathf.Max(playerPositions_x);
            min_y = Mathf.Min(playerPositions_y);
            max_y = Mathf.Max(playerPositions_y);
            min_z = Mathf.Min(playerPositions_z);
            max_z = Mathf.Max(playerPositions_z);

            averageDistance = new Vector3(min_x + (Mathf.Abs(max_x - min_x) / 2), min_y + (Mathf.Abs(max_y - min_y) / 2), min_z + (Mathf.Abs(max_z - min_z) / 2));

            m_agent.SetDestination(averageDistance);
            if (!m_agent.CalculatePath(averageDistance, m_agent.path))
            {
                m_agent.SetDestination(HerdMates[0].transform.position);
            }

        }
        else
        {
            currentActivityTime += 1;
        }
    }

    void Leave()
    {

        currentWanderPoint = leavePoint;
        m_agent.SetDestination(LeavePoints[currentWanderPoint].gameObject.transform.position);
    
        if (m_agent.remainingDistance <= .1f)
        {
            StartCoroutine(destroyYourself());
        }
    }

    IEnumerator destroyYourself()
    {

        bool delete = true;
        float endValue = 0;
        float startValue = m_SpriteRenderer.color.a;

        while (delete)
        {
            float newAlpha = Mathf.Lerp(startValue, endValue, .05f);
            m_SpriteRenderer.color = new Color(m_SpriteRenderer.color.r, m_SpriteRenderer.color.g, m_SpriteRenderer.color.b, newAlpha);
            yield return new WaitForEndOfFrame();

            if (m_SpriteRenderer.color.a < 0.1f)
            {
                Destroy(this.gameObject);
            }
        }
    }
}

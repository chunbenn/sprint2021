﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class LookAtCamera : MonoBehaviour
{
    NavMeshAgent m_agent;
    SpriteRenderer m_SpriteRenderer;

    public Camera cameraToLookAt;
    Vector3 target_point;
    private void Awake()
    {
        cameraToLookAt = FindObjectOfType<Camera>();
        m_agent = GetComponent<NavMeshAgent>();
        m_SpriteRenderer = GetComponent<SpriteRenderer>();
    }



    // Update is called once per frame
    void Update()
    {
        target_point = cameraToLookAt.transform.position;
        target_point.y = 0;

        //transform.LookAt(target_con.transform.position,transform.up);
        transform.LookAt(target_point);

        Vector3 navMeshDir = m_agent.destination - this.transform.position;
        Vector3 camerDir = Vector3.zero -  new Vector3 (cameraToLookAt.transform.position.x, 0, cameraToLookAt.transform.position.z);
        float angle = Vector3.SignedAngle(navMeshDir, camerDir, Vector3.up);

        if (angle > 0)
        {
            m_SpriteRenderer.flipX = true;
        }
        else
        {
            m_SpriteRenderer.flipX = false;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraPositions : MonoBehaviour
{
    [SerializeField]
    Transform[] cameraPositions;

    [SerializeField]
    int currentPosition;
    Transform holdingPosition;

    bool zooming;
    public float ScrollSpeed = 2.5f;
    Camera m_Camera;

    Vector3 initalPosition;
    Vector3 endPosition;
    [SerializeField]
    float deadzone;
    [SerializeField]
    float maxDistance;

    private void Awake()
    {
        m_Camera = GetComponent<Camera>();
    }
    private void Update()
    {

        if (Input.mouseScrollDelta.y > 0.25f)
        {
            if (!zooming)
            {
                StopCoroutine(ZoomIn());
                StartCoroutine(ZoomIn());
                zooming = true;
            }
        }

        if (Input.mouseScrollDelta.y < -0.25f)
        {
            if (zooming)
            {
                StopCoroutine(ZoomOut());
                StartCoroutine(ZoomOut());
                zooming = false;
            }
        }


        if (Input.GetMouseButtonDown(1))
        {
            StartCoroutine(dragToMove());
        }
  
        Scrolling();

    }

    IEnumerator ZoomIn()
    {
        float timer = 0;
        while (timer < 3f)
        {
            m_Camera.orthographicSize = Mathf.Lerp(m_Camera.orthographicSize, 3.75f, 0.01f);
            yield return new WaitForEndOfFrame();
            timer += Time.deltaTime;
        }
    }
    IEnumerator ZoomOut()
    {
        float timer = 0;
        while (timer < 3f)
        {
            m_Camera.orthographicSize = Mathf.Lerp(m_Camera.orthographicSize, 8.5f, 0.01f);
            yield return new WaitForEndOfFrame();
            timer += Time.deltaTime;
        }
    }

    IEnumerator RotateCamera()
    {
        float timer = 0;
        while (timer < 5f)
        {
            holdingPosition = cameraPositions[currentPosition];
            transform.position = Vector3.Lerp(transform.position, holdingPosition.transform.position, .005f);
            transform.rotation = Quaternion.Lerp(transform.rotation, holdingPosition.transform.rotation, .005f);
            yield return new WaitForEndOfFrame();
            timer += Time.deltaTime;
        }
        StopAllCoroutines();
    }

    

    IEnumerator dragToMove()
    {

        initalPosition = new Vector3 (Input.mousePosition.x, 0, Input.mousePosition.y);
        while (Input.GetMouseButton(1))
        {
            endPosition = new Vector3(Input.mousePosition.x, 0, Input.mousePosition.y);
            yield return new WaitForEndOfFrame();
        }

        initalPosition = Vector3.zero;
        endPosition = Vector3.zero;
    }

    void Scrolling()
    {

        Vector3 cameraMovement = Vector3.zero;
        float scrollingSpeed = 0;
        //Direction

        //Vector2 directionFromScreenCenter = new Vector2(Input.mousePosition.x, Input.mousePosition.y) - new Vector2(Screen.width / 2, Screen.height / 2);
        Vector3 directionFromScreenCenter = (endPosition) - initalPosition;

        float distanceFromScreenCenter = Vector3.Distance(initalPosition, endPosition);
        distanceFromScreenCenter = Mathf.Clamp(distanceFromScreenCenter, 0, maxDistance);


        if (distanceFromScreenCenter >= deadzone)
        {

            cameraMovement = directionFromScreenCenter.normalized;
            scrollingSpeed = ScrollSpeed * (1 + Mathf.Lerp(0, 1, distanceFromScreenCenter / maxDistance));
        }


        transform.position = Vector3.Lerp(transform.position, transform.position + cameraMovement.normalized * Time.deltaTime * scrollingSpeed, 0.1f);
        Vector3 tempPos = transform.position;

        if (zooming)
        {
            tempPos.x = Mathf.Clamp(transform.position.x, -60, -10);
            tempPos.z = Mathf.Clamp(transform.position.z, -60, -10);

        }
        else
        {
            tempPos.x = Mathf.Clamp(transform.position.x, -50, -20);
            tempPos.z = Mathf.Clamp(transform.position.z, -50, -20);
        }
        transform.position = tempPos;
    }
}

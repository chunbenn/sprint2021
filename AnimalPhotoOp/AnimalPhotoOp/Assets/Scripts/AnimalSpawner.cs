﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimalSpawner : MonoBehaviour
{

    // The GameObject to instantiate.
    public GameObject animalBase;

    float timer;
    [SerializeField]
    float spawnInterval;

    public GameObject PoolAActive;
    public GameObject PoolBActive;
    public GameObject PoolCActive;
    public GameObject PoolDActive;
    public GameObject PoolEActive;
    public GameObject PoolFActive;

    //
    public AnimalBaseScriptableObject[] animalPoolA;
    public AnimalBaseScriptableObject[] animalPoolB;
    public AnimalBaseScriptableObject[] animalPoolC;
    public AnimalBaseScriptableObject[] animalPoolD;
    public AnimalBaseScriptableObject[] animalPoolE;
    public AnimalBaseScriptableObject[] animalPoolF;

    [SerializeField]
    Transform[] SpawnPoints;

    private void Awake()
    {
        SpawnEntities();
    }

    private void Update()
    {
       timer += Time.deltaTime;

        if (timer >= spawnInterval)
        {
            timer = 0;
            SpawnEntities();
        }
    }

    void SpawnEntities()
    {
            // Creates an instance of the prefab at the current spawn point.
         GameObject currentEntity = Instantiate(animalBase, SpawnPoints[Random.Range(0,SpawnPoints.Length)].transform.position,Quaternion.identity);

        int totalPoolLength = 0;
        if (PoolAActive.activeSelf)
        {
            totalPoolLength += animalPoolA.Length;
        }
        if (PoolBActive.activeSelf)
        {
            totalPoolLength += animalPoolB.Length;
        }
        if (PoolCActive.activeSelf)
        {
            totalPoolLength += animalPoolC.Length;
        }
        if (PoolDActive.activeSelf)
        {
            totalPoolLength += animalPoolC.Length;
        }
        if (PoolEActive.activeSelf)
        {
            totalPoolLength += animalPoolC.Length;
        }
        if (PoolFActive.activeSelf)
        {
            totalPoolLength += animalPoolC.Length;
        }

        int AnimalToSpawn = Random.Range(0, totalPoolLength);
        int leftOverSpawn = 0;

        if (PoolAActive.activeSelf) 
        {
            if (AnimalToSpawn < animalPoolA.Length)
            {
                currentEntity.GetComponent<AnimalBehaviour>().AnimalValues = animalPoolA[AnimalToSpawn];
            }
            leftOverSpawn += animalPoolA.Length;
        } 

        if (PoolBActive.activeSelf)
        {
            if (AnimalToSpawn < (animalPoolB.Length + leftOverSpawn) && AnimalToSpawn >= leftOverSpawn)
            {
                currentEntity.GetComponent<AnimalBehaviour>().AnimalValues = animalPoolB[AnimalToSpawn - leftOverSpawn];
            }
            leftOverSpawn += animalPoolB.Length;
        } 

         if (PoolCActive.activeSelf)
        {
            if (AnimalToSpawn < (animalPoolC.Length + leftOverSpawn) && AnimalToSpawn >= leftOverSpawn)
            {
                currentEntity.GetComponent<AnimalBehaviour>().AnimalValues = animalPoolC[AnimalToSpawn - leftOverSpawn];
            }
            leftOverSpawn += animalPoolC.Length;
        }
        if (PoolDActive.activeSelf)
        {
            if (AnimalToSpawn < (animalPoolD.Length + leftOverSpawn) && AnimalToSpawn >= leftOverSpawn)
            {
                currentEntity.GetComponent<AnimalBehaviour>().AnimalValues = animalPoolD[AnimalToSpawn - leftOverSpawn];
            }
            leftOverSpawn += animalPoolD.Length;
        }
        if (PoolEActive.activeSelf)
        {
            if (AnimalToSpawn < (animalPoolE.Length + leftOverSpawn) && AnimalToSpawn >= leftOverSpawn)
            {
                currentEntity.GetComponent<AnimalBehaviour>().AnimalValues = animalPoolE[AnimalToSpawn - leftOverSpawn];
            }
            leftOverSpawn += animalPoolE.Length;
        }
        if (PoolFActive.activeSelf)
        {
            if (AnimalToSpawn < (animalPoolF.Length + leftOverSpawn) && AnimalToSpawn >= leftOverSpawn)
            {
                currentEntity.GetComponent<AnimalBehaviour>().AnimalValues = animalPoolF[AnimalToSpawn - leftOverSpawn];
            }
            leftOverSpawn += animalPoolF.Length;
        }
        currentEntity.GetComponent<AnimalBehaviour>().SetValues();

    }

}

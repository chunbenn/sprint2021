﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Animal", menuName = "ScriptableObjects/AnimalBaseScriptableObject", order = 1)]
public class AnimalBaseScriptableObject : ScriptableObject
{

    public string prefabName;
    public float moveSpeed;
    public Sprite m_sprite;
    public float visitTime;
    public float activityTime;
    public int decorationUnlock;
    public AnimatorOverrideController baseAnimations;
    public int animalNumber;
}

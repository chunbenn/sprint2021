﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HerdDetection : MonoBehaviour
{

    AnimalBehaviour m_animalBehaviour;

    private void Awake()
    {
        m_animalBehaviour = GetComponentInParent<AnimalBehaviour>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponentInParent<AnimalBehaviour>() != null)
        {
            if (other.GetComponentInParent<AnimalBehaviour>().name == GetComponentInParent<AnimalBehaviour>().name)
            {
                if (other.GetComponentInParent<AnimalBehaviour>().hasHerded == false && m_animalBehaviour.hasHerded == false)
                {
                    m_animalBehaviour.FoundHerdMate(other.transform.parent.gameObject);
                }
            }
        }
    }


}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerVariables : MonoBehaviour
{


    [SerializeField]
    bool[] decorationUnlockable;
    [SerializeField]
    GameObject[] decorationToUnlock;

    
    Camera m_camera;
    public Vector3 worldPosition;

    [SerializeField]
    public bool[] foundAnimals;

    UIAudio m_audio;
    private void Awake()
    {
        m_camera = GetComponent<Camera>();
        m_audio = GetComponent<UIAudio>();
    }

    void Update()
    {

        Plane plane = new Plane(Vector3.up, 0);

        float distance;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (plane.Raycast(ray, out distance))
        {
            worldPosition = ray.GetPoint(distance);
        }

      
        Debug.DrawRay(worldPosition, Vector3.up);
        if (Input.GetKeyDown(KeyCode.Space))
        {
            detectAnimals();
        }
    }

    public void detectAnimals()
    {
        AnimalBehaviour[] checkObjects = FindObjectsOfType<AnimalBehaviour>();

        for (int i = 0; i < checkObjects.Length; i++)
        {
            if (m_camera.WorldToViewportPoint(checkObjects[i].gameObject.transform.position).x > 0 && m_camera.WorldToViewportPoint(checkObjects[i].gameObject.transform.position).x < 1 && m_camera.WorldToViewportPoint(checkObjects[i].gameObject.transform.position).y > 0 && m_camera.WorldToViewportPoint(checkObjects[i].gameObject.transform.position).y < 1)
            {
                animalCheck(checkObjects[i].gameObject);
                foundAnimals[checkObjects[i].GetComponent<AnimalBehaviour>().animalNumber] = true;
            }
        }

    }


    public void tryUnlockDecoration(int decorationToTry)
    {
        if (decorationUnlockable[decorationToTry])
        {
            if (!decorationToUnlock[decorationToTry].GetComponent<Decoration>().m_decoration.activeSelf)
            {
                decorationToUnlock[decorationToTry].GetComponentInChildren<ParticleSystem>().Play();
                m_audio.playRandomAudio();
            }
            decorationToUnlock[decorationToTry].GetComponent<Decoration>().m_decoration.SetActive(true);
            decorationToUnlock[decorationToTry].GetComponent<Decoration>().decorationPlaceHolder.SetActive(false);
            if (!decorationToUnlock[decorationToTry].GetComponent<Decoration>().m_decoration.activeSelf)
            {
                decorationToUnlock[decorationToTry].GetComponentInChildren<ParticleSystem>().Play();
            }
        }
    }


    public void animalCheck(GameObject animalToCheck)
    {

        decorationUnlockable[animalToCheck.GetComponent<AnimalBehaviour>().AnimalValues.decorationUnlock] = true;

    }
}




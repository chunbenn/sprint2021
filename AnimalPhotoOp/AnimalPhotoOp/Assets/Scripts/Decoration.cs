﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Decoration : MonoBehaviour
{

    [SerializeField]
    int decorationNumber;
    [SerializeField]
    public GameObject m_decoration;
    [SerializeField]
    GameObject hoverOver;
    [SerializeField]
    public GameObject decorationPlaceHolder;

    private void OnMouseEnter()
    {
        if (m_decoration.activeSelf)
        {

        }
        else
        {

            hoverOver.SetActive(true);
        }
    }

    private void OnMouseExit()
    {

        hoverOver.SetActive(false);
    }

    private void OnMouseDown()
    {
        FindObjectOfType<PlayerVariables>().tryUnlockDecoration(decorationNumber);
    }

}
